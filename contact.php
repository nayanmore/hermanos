<?php

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    echo "Only Post Request Accepted.";
    die();
}

require 'vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

use Respect\Validation\Validator as v;


// Recaptcha
// $url = 'https://www.google.com/recaptcha/api/siteverify';

// $parameters = [
//     'secret' => '6LfZ5t0cAAAAAF0N_Wt6vr8th0aW2bTOiZxxhhxT',
//     'response' => $_POST['g-recaptcha-response']
// ];

// $qs = http_build_query($parameters);
// $curl_request = "{$url}?{$qs}";

// $curl = curl_init();
// curl_setopt_array($curl, array(CURLOPT_URL => $curl_request, CURLOPT_RETURNTRANSFER => 1));

// $response = curl_exec($curl);                
// $data = json_decode($response);

// curl_close($curl);

// if(!$data->success){
//     echo "Captcha Verfication Failed";
//     http_response_code(422);
//     die();
// }



// Validations
// if(v::stringVal()->stringType()->length(3, 120)->notEmpty()->validate($_POST['name'])){
//     $name = ucwords($_POST['name']);
// }
// else{
//     echo "Invalid Name. Should Be Between 3 to 120 Characters.";
//     http_response_code(422);
//     die();
// }

// if(v::email()->notEmpty()->validate($_POST['email'])){
//     $email = ucwords($_POST['email']);
// }
// else{
//     echo "Invalid Email.";
//     http_response_code(422);
//     die();
// }

// if(v::stringVal()->stringType()->length(5, 120)->notEmpty()->validate($_POST['company'])){
//     $company = ucwords($_POST['company']);
// }
// else{
//     echo "Invalid Company Name. Should Be Between 5 to 120 Characters";
//     http_response_code(422);
//     die();
// }

// if(v::stringVal()->stringType()->length(2, 120)->notEmpty()->validate($_POST['title'])){
//     $title = ucwords($_POST['title']);
// }
// else{
//     echo "Invalid Title. Should Be Between 2 to 120 Characters";
//     http_response_code(422);
//     die();
// }

// if(v::stringVal()->stringType()->length(10, 120)->notEmpty()->validate($_POST['message'])){
//     $message = ucwords($_POST['message']);
// }
// else{
//     echo "Invalid Message. Should Be Between 10 to 120 Characters";
//     http_response_code(422);
//     die();
// }

$name = $_POST["name"];
$email = $_POST["email"];
$phone = $_POST["phone"];
$company = $_POST["company"];
$message = $_POST["message"];
// Mail
$mail = new PHPMailer(true);

try {
    // $mail->SMTPDebug = false;
    // $mail->isSMTP();
    // $mail->Host       = 'mail.jeinv.com';
    // $mail->SMTPAuth   = true;
    // $mail->Username   = 'mails@ideamagix.com';
    // $mail->Password   = 'bW8fW$%DeyEX';
    // $mail->SMTPSecure = 'ssl';
    // $mail->Port       = 465;

    $mail->SMTPDebug = false;
    $mail->isSMTP();
    $mail->Host       = 'smtp.mailtrap.io';
    $mail->SMTPAuth   = true;
    $mail->Username   = '0960c2eaafc300';
    $mail->Password   = '1e770611e41ba7';
    $mail->SMTPSecure = false;
    $mail->Port       = 25;



    //Recipients   
    $mail->setFrom('mails@ideamagix.com', 'Hermanos');
    $mail->addAddress('rishikesh.ideamagix@gmail.com', 'Hermanos');
    $mail->addAddress('nayan.ideamagix@gmail.com', 'Hermanos');
    // $mail->addAddress('a.bhat@pronuvaconsulting.com', 'Hermanos');
    // $mail->addAddress('t.bhat@pronuvaconsulting.com', 'Hermanos');
    // $mail->addAddress('info@pronuvaconsulting.com', 'Hermanos');

    //Content
    $mail->isHTML(true);
    $mail->Subject = 'New Enquiry Form Message From Your ProNuva Website';
    $mail->Body    = '<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Title</title>
        <style>
            body, table{
                width: 100%;
            }
            table{
                border: 1px solid #ccc;
                padding: 2px;
            }
            td, th{
                border: 1px solid #ccc;
                padding: 5px;
            }
        </style>
    </head>
    <body>
        <p>Hi, you have recieved a new enquiry from ProNuva Website.</p>
        <table>
            <tr>
                <th>Name</th>
                <td>'. $name .'</td>
            </tr>
            <tr>
                <th>Company</th>
                <td>'. $company .'</td>
            </tr>
            <tr>
                <th>Phone</th>
                <td>'.$phone.'</td>
            </tr>
            <tr>
                <th>Email</th>
                <td>'.$email.'</td>
            </tr>
            <tr>
                <th> Message </th>
                <td>'.$message.'</td>
            </tr>
        </table>
        <p>Thanks.</p>
    </body>
    </html>';

    // For clients that won't support html.
    $mail->AltBody = 'Hi, you have recieved a new enquiry from your website. Name :- '.$name.', Email :- '.$email.', Comapny :- '.$company.', Phone :- '.$phone.', Message :- '.$message.', Thanks';

    if($mail->send()) {
        echo '1';
    } else {
        echo '0';
    }
    exit;
} 
catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";

    $log  = "User: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").PHP_EOL.
        "Log: ".($mail->ErrorInfo).PHP_EOL.
        "-------------------------".PHP_EOL;
    //Save string to log, use FILE_APPEND to append.
    file_put_contents('./logs/log_'.date("j.n.Y").'.log', $log, FILE_APPEND);
    die();
}

?>