const enqBtn =  document.querySelector('#enquiryForm');
let
custName = document.querySelector('#name'),
email = document.querySelector('#email'),
phone = document.querySelector('#phone'),
company = document.querySelector('#company'),
message = document.querySelector('#message'),
inputs = document.getElementsByTagName('input'),
alert = document.querySelector('.alert-popup');

let emailRegex =  /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;


enqBtn.addEventListener('click', () => {
    
    for (const inp of inputs) {
        inp.style.border = '1px solid #cccccc';
    }
    let hasError = false;

    if(custName.value.trim() === '') {
        hasError = true;
        custName.style.border = '1px solid red';
    }
    if(email.value.trim() === '') {
        hasError = true;
        email.style.border = '1px solid red';
    }
    if(!emailRegex.test(email.value)) {
        hasError = true;
        email.style.border = '1px solid red';
    };
    if(phone.value.trim() === '') {
        hasError = true;
        phone.style.border = '1px solid red';
    }
    if(message.value.trim() === '') {
        hasError = true;
        message.style.border = '1px solid red';
    }

    if(hasError) {
        return false;
    }else {
        message.style.border = '1px solid #cccccc';

        let btnText = enqBtn.innerHTML;
        enqBtn.innerHTML = '<span class="ml-2 p-0">Submitting...</span>';

        const formData = new FormData()
        formData.append('name', custName.value);
        formData.append('email', email.value);
        formData.append('phone', phone.value);
        formData.append('company', company.value);
        formData.append('message', message.value);

        const fetchData = () => {
            
                fetch('contact.php', {
                    method: 'POST',
                    header: {
                        "Content-Type": "application/x-www-form-urlencoded",
                    },
                    body: formData
                }).then(res => {
                    if(res.status === 200) {
                        enqBtn.innerHTML = btnText;
                        for(let i of inputs) {
                            i.value = '';
                        }
                        alert.style.display = 'block';
                        setTimeout(() => {
                            alert.style.display = 'none';
                        } ,5000)
                        
                    }else {
                        enqBtn.innerHTML = btnText;
                        alert.innerHTML = "<h6> Something went wrong, Please try again later! </h6>";
                        alert.style.color = "#721C24 !important";
                        alert.style.backgroundColor = "#F8D7DA";
                        alert.style.display = 'block';
                        setTimeout(() => {
                            alert.style.display = 'none';
                        } ,5000)
                    }
                })

                
            
          
        }
        fetchData()

        



    }





})
